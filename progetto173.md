Progetto173
========================================================
author: Domenico Viola
date: 16/09/2015

Introduzione
========================================================

In questa presentazione riporteremo le parti maggiormente significative realizzate attraverso l'utilizzo del linguaggio di programmazione R. Presenteremo le funzioni summary (), min(), max(), mean(), var(), sd(),length(),sum(), plot() e barplot(), specificando di seguito quello che esprimono tali funzioni e mostrando altresì i risultati generati.



Funzione read.csv() per caricare dati progetto.
========================================================

```r
setwd("C:/Users/Domenico/Desktop/Progetto173")
data <- read.csv("Progetto173.csv", header=T, dec=",",sep=",")
```

Funzione summary()
========================================================
Determinare informazione sulle variabili dell'oggetto data


```r
summary(data)
```

```
          Categoria                                   Tipologia.di.spesa
 Spese correnti:6   acquisti di beni di consumo e materie prime:1       
                    imposte e tasse                            :1       
                    oneri straordinari della gestione corrente :1       
                    personale                                  :1       
                    prestazioni di servizi                     :1       
                    utilizzo di beni di terzi                  :1       
      Euro        
 Min.   :   1550  
 1st Qu.:  53911  
 Median :  62150  
 Mean   : 230047  
 3rd Qu.: 125750  
 Max.   :1056316  
```

Funzioni min(), max()
========================================================
Indici statistici di sintesi, tra cui valore minimo e massimo


```r
min(data$Euro)
```

```
[1] 1550
```

```r
max(data$Euro)
```

```
[1] 1056316
```

Funzioni mean(), var(), sd()
========================================================
Indici statistici di sintesi, tra cui media, varianza e
scarto quadratico media


```r
mean(data$Euro)
```

```
[1] 230046.8
```

```r
var(data$Euro)
```

```
[1] 165995659884
```

```r
sd(data$Euro)
```

```
[1] 407425.6
```

Funzioni length() e sum()
========================================================
Indici statistici di sintesi, tra cui numero e somma totale degli elementi


```r
length(data$Euro)
```

```
[1] 6
```

```r
sum(data$Euro)
```

```
[1] 1380281
```


Funzione plot()
========================================================
Andamento dei valori in euro mediante grafico lineare

![plot of chunk unnamed-chunk-6](progetto173-figure/unnamed-chunk-6-1.png) 

Funzione barplot()
========================================================
grafico a barre, valori in euro rispetto alle categorie

![plot of chunk unnamed-chunk-7](progetto173-figure/unnamed-chunk-7-1.png) 

Funzione pie()
========================================================
Grafico (opzionale) a torta, valori in euro rispetto alle tipologie di spesa

![plot of chunk unnamed-chunk-8](progetto173-figure/unnamed-chunk-8-1.png) 
